# README #

# NOTIFICACIONES #

1) Para la interfaz, colocamos un botón en el centro, con los respectivos constraints y conectamos ese botón al ViewController como una acción.

2) En ViewController, importamos UserNotifications.

3) En viewDidLoad() se colocará el código en el que se pedirá permiso al usuario para mostrarle notificaciones.

4) Este resultado se almacenará en una variable de tipo Bool llamada isGrantedNotificationAccess.

5) Dentro de la acción creada mediante la conexión al botón, colocamos un if para asegurarnos que las notificaciones sean recibidas solo si el usuario aceptó.

6) Empezamos a colocar el contenido de la notificación.

 - Título
 - Subtítulo
 - Cuerpo
 - Identificador de categoría

7) Colocamos un trigger que se activa en 10 segundos cuando la aplicación está cerrada.

8) Unimos el contenido y el trigger junto con un identificador dentro de un UNNOtificationRequest.
 
 - El identificador es importante para evitar conflictos si dos notificaciones se muestran simultáneamente.

9) Añadimos la notificación al Notification Center actual.




