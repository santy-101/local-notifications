//
//  ViewController.swift
//  Notifications
//
//  Created by Santiago Lema on 3/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate{
    
    var isGrantedNotificationAccess:Bool = false

    @IBAction func send10SecNotification(_ sender: UIButton) {
        
        
        if isGrantedNotificationAccess{
            //add notification code here
            
            let content = UNMutableNotificationContent()
            content.title = "10 Second Notification Demo"
            content.subtitle = "From MakeAppPie.com"
            content.body = "Notification after 10 seconds - Your pizza is Ready!!"
            content.categoryIdentifier = "message"
            
            
            let trigger = UNTimeIntervalNotificationTrigger(
                timeInterval: 10.0,
                repeats: false)
            
            let request = UNNotificationRequest(
                identifier: "10.second.message",
                content: content,
                trigger: trigger)
                
                UNUserNotificationCenter.current().add(
                    request, withCompletionHandler: nil)
        }

        }
        
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in
                self.isGrantedNotificationAccess = granted
        
    }
        )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

